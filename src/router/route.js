import React, { useEffect, useState } from 'react';
import { Route, Switch } from "react-router";
import AddTodo from "../container/AddTodo";
import ListTodo from "../container/TodoList";
import TodoApp from "../container/TodoApp";


export const routeLinks = {
    root: '/',
    addTodo: "/addtodo",
    listTodo: "/listtodo",
}
function AppRouter(props) {
    return (
        <div className={"routes-wrapper"}>
            <Switch>
                <Route
                    exact
                    path={routeLinks.root}
                    component={TodoApp}
                />
                <Route
                    exact
                    path={routeLinks.addTodo}
                    component={AddTodo}
                />
                <Route
                    exact
                    path={routeLinks.listTodo}
                    component={ListTodo}
                />

            </Switch>
        </div>
    );
}

export default AppRouter;
