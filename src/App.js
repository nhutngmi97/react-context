import React, { useState, useReducer } from 'react';
import { TodoContext } from './context/TodoContext'
import AppRouter from './router/route';
import './App.css';
import TodoReducer, { initalTodo } from './context/todoReducer';

function App() {
  const todoDataState = useReducer(TodoReducer, initalTodo)
  console.log('todoDataState',todoDataState)
  return (
    <div>
      <TodoContext.Provider value={todoDataState}>
        <AppRouter />
      </TodoContext.Provider>
    </div>

  );
}

export default App;
