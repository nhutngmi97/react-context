import React, { useState } from 'react';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import { routeLinks } from "../router/route";
import { useHistory } from "react-router-dom";

import './style.css';

function TodoApp() {
    let history = useHistory();
    const handleNav = () => {
        history.push(routeLinks.listTodo)
    }
    return (
        <div className={'todoapp-content  '}>
            <button onClick={handleNav} className={'btn btn-primary'}>
                Todo App
            </button>
        </div >
    );
}
export default TodoApp;
