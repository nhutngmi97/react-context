import React, { useContext, useState } from 'react';
import TodoItem from '../components/TodoItem';
import PropTypes, { number } from "prop-types";
import { TodoContext } from '../context/TodoContext';
import './style.css';
import { useHistory } from 'react-router';
import { routeLinks } from '../router/route';

function TodoList() {
    const history = useHistory();
    const [todoState, todoDispatch] = useContext(TodoContext);
    let { list } = todoState;
    console.log(list);
    
    return (
        <div className={'todolist-content'}>
            {
                list && list.map((item, key) => {

                    return (<TodoItem id={key} todoTitle={item.title} todoContent={item.content} />)
                })
            }
            <button className='btn btn-primary' onClick={() => {
                history.push(routeLinks.addTodo)
            }}>Back to add</button>
        </div>
    );
};

export default TodoList;
