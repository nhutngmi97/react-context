import React, { useContext, useRef } from 'react';
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import { routeLinks } from '../router/route';
import { TodoContext } from '../context/TodoContext';
import { addTodo } from '../context/todoAction';


function AddTodo(props) {
    const [todoState, todoDispatch] = useContext(TodoContext);
    let todoObj = { title: '', content: '' };
    const history = useHistory();
    const inputElTitle = useRef(null);
    const inputElContent = useRef(null);
    const onHandleAddTodo = () => {
        let tempObj = { ...todoObj };
        tempObj.title = inputElTitle.current.value;
        tempObj.content = inputElContent.current.value;
        todoObj={...tempObj};
        todoDispatch(addTodo(todoObj))
        history.push(routeLinks.listTodo)
    }
    return (
        <div className={'addtodo-content'}>
            <input ref={inputElTitle} className={'addtodo-input'} />
            <input ref={inputElContent} className={'addtodo-input'} />
            <button className={'addtodo-button btn btn-primary'} onClick={onHandleAddTodo}>Add Todo</button>
        </div>
    );
}
AddTodo.propTypes = {
    handleAddTodo: PropTypes.func
};
export default AddTodo;
