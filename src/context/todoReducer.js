import *as TodoConstant from './todoConstant';
export const initalTodo = { list: [] };
function TodoReducer(state, action) {
  let temp;
  let newList;
  switch (action.type) {
    case TodoConstant.TODO_ADD: {
      temp = { ...state };
      let { list = [] } = temp;
      list.push({ title: action.title, content: action.content })
      newList = [...list];
      return { ...temp, list: [...newList] };
    }
    case TodoConstant.TODO_DELETE: {
      temp = { ...state };
      let { list = [] } = temp;
      list.splice(action.id, 1);
      newList = list;
      return { ...temp, list: newList };
    }
    case TodoConstant.TODO_EDIT: {
      temp = { ...state };
      console.log(action.id)
      let { list = [] } = temp
      list[action.id] = Object.assign(list[action.id], action.value)
      newList = [...list];
      return { ...temp, list: [...newList] };
    }
    default:
      return state;
  }
}

export default TodoReducer;
