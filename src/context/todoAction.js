import { TODO_ADD, TODO_DELETE, TODO_EDIT } from "./todoConstant"

export const addTodo = (todoObj) => {
    return {
        type: TODO_ADD,
        title: todoObj.title,
        content: todoObj.content
    }
}
export const deleteTodo = (id) => {
    return {
        type: TODO_DELETE,
        id
    }
}
export const editTodo = (todoObj, id) => {
    return {
        type: TODO_EDIT,
        value: todoObj,
        id
    }
}