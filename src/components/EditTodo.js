import React, { useContext, useRef } from 'react';
import { useHistory } from "react-router-dom";
import { TodoContext } from '../context/TodoContext';
import { editTodo } from '../context/todoAction';

function EditTodo(props) {
    const [todoState, todoDispatch] = useContext(TodoContext);
    let todoObj = { title: '', content: '' };
    const history = useHistory();
    const inputElTitle = useRef(null);
    const inputElContent = useRef(null);    const onHandleEditTodo = () => {
        let tempObj = { ...todoObj };
        tempObj.title = inputElTitle.current.value;
        tempObj.content = inputElContent.current.value;
        todoObj = { ...tempObj };
        todoDispatch(editTodo(todoObj,props.id))
        props.closeEdit()
    }

    return (
        <div className={'addtodo-content'}>
            <input ref={inputElTitle} className={'addtodo-input'} />
            <input ref={inputElContent} className={'addtodo-input'} />
            <button className={'addtodo-button btn btn-primary'} onClick={onHandleEditTodo}>Edit Todo</button>
        </div>
    );
}

export default EditTodo;