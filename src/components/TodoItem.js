import React, { useContext, useState } from 'react';
import PropTypes from "prop-types";
import './style.css';
import { TodoContext } from '../context/TodoContext';
import { deleteTodo } from '../context/todoAction';
import { useHistory } from 'react-router';
import { routeLinks } from '../router/route';
import EditTodo from './EditTodo';

function TodoItem(props) {
  const [isShow, setIsShow] = useState(false);
  const history = useHistory();
  const [todoState, todoDispatch] = useContext(TodoContext);
  let showStyle = {};
  const {
    todoTitle,
    todoContent,
    id
  } = props;
  let showEdit = [];


  function onHandleDelete() {
    todoDispatch(deleteTodo(id));
  }
  function openEdit() {
    if (isShow == false) {
      setIsShow(!isShow)
    }
  }
  function closeEdit() {
    setIsShow(false)
  }
  if (isShow) {
    showEdit.push(<EditTodo id={id} closeEdit={closeEdit} />)
  } else {
    showEdit = [];
  }
  return (
    <div className={'todoitem-content'}>
      <h2>{todoTitle}</h2>
      <h4>{todoContent}</h4>
      <button className={'edit-btn btn btn-warning'} onClick={openEdit}>edit</button>
      <button className={'delete-btn btn btn-danger'} onClick={onHandleDelete}>delete</button>
      {showEdit}
    </div>);
}
TodoItem.propTypes = {
  todoTitle: PropTypes.string,
  todoContent:PropTypes.string
};
export default TodoItem;
